import { createRouter, createWebHistory } from 'vue-router';
const home = () => import('../views/Home.vue');

const routes = [
  {
    path: '/',
    name: 'home',
    component: home,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
